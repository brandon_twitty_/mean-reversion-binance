const Bottleneck = require("bottleneck");
const limiter = new Bottleneck({
    maxConcurrent: 1,
    minTime: 1000
});
class Utils{
    constructor() {
    }
    getFormattedDate() {
        var date = new Date();
        var month = date.getMonth() + 1;
        var day = date.getDate();
        var hour = date.getHours();
        var min = date.getMinutes();
        var sec = date.getSeconds();
        month = (month < 10 ? "0" : "") + month;
        day = (day < 10 ? "0" : "") + day;
        hour = (hour < 10 ? "0" : "") + hour;
        min = (min < 10 ? "0" : "") + min;
        sec = (sec < 10 ? "0" : "") + sec;
        var str = date.getFullYear() + "-" + month + "-" + day + "_" +  hour + ":" + min + ":" + sec;
        console.log(str);
        return str;
    }
    async getSma(s, i){
        let usableSymbol = s + '/USDT'

        let smaData = await limiter.schedule(() =>sma(9, "close", "binance", usableSymbol, i, false))
        let lastSMANinecandle = smaData[smaData.length - 1]
        global.smaNine = lastSMANinecandle
        console.log(usableSymbol, 'in sma 9')
        //  global.technicalIndicators.sma9 = smaData[smaData.length - 1]
        // global.tradingData.sma9 = lastSMANinecandle
        return lastSMANinecandle
    }
}

module.exports = Utils;
