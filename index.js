const express = require('express');
const app = express();
require('dotenv').config()
const _ = require('lodash');
const dotenv = require('dotenv')
const Bottleneck = require("bottleneck");
const fetch = require('node-fetch')
const Binanceus = require('node-binance-us-api');
const { $, gt, multiply,divide, in$, add } = require('moneysafe');
const { $$, subtractPercent, addPercent } = require('moneysafe/ledger');
const Promise = require('promise');
const Utils = require("./common/utils");
const ccxt = require ('ccxt')
const binanceUS = new Binanceus().options({
    APIKEY: process.env.APIKEY,
    APISECRET: process.env.APISECRET,
    useServerTime: true,
    recvWindow: 60000, // Set a higher recvWindow to increase response timeout
    verbose: true,
});
const taapiSecret = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImJyYW5kb24udHdpdHR5QGNvZGVzdGxvdWlzLmNvbSIsImlhdCI6MTYyNjk5NzI0OCwiZXhwIjo3OTM0MTk3MjQ4fQ.edCjkzPR9x2dUeQw4mqMpjRnrqtok4zsoVXm9kEumdo'
const limiter = new Bottleneck({
    maxConcurrent: 1,
    minTime: 1000
});
global.tradingData ={
    symbol:{},
    sellPrice:{},
    buyPrice:{},
    closed:{},
    amount:{},
    balance:{},
    orderType:{},
    tradeId:{}
}
global.myBalances ={
    buyingPower: {},
    balance:{}
}


let binanceSymbol = process.env.SYMBOL + 'USD'
global.tradingData.symbol = process.env.SYMBOL
const taapi = process.env.TAAPIKEY
let t = new Date
const appUtils = new Utils()
appUtils.getFormattedDate()
const rawUtcTimeNow = (Math.floor(t.getTime()))
const intervals =['1m']
const asset = process.env.SYMBOL
async function getBuyingPower() {
    if (global.tradingData.symbol !== null) {
        await binanceUS.balance((error, balances) => {
            let money = balances['USD'];
            if(money !== undefined){
                let obj = $.of(money)
                obj.available = money.available
                obj.onOrder = money.onOrder
                obj.total = obj.available + obj.onOrder
                console.log(obj.available,'buying power')
                global.myBalances.buyingPower = parseFloat(obj.available)
                return obj.available
            }

        })

    }
}
function buyingPowerPromise(){
    new Promise((Resolve, Reject) =>{
        getBuyingPower().then(data =>{
            if(data){
                Resolve(data)
            } else {
                Reject(data)
            }
        })
    })
}
async function cancelAllOpenOrders(){
    let binanceSymbol = asset + 'USD'
    await binanceUS.cancelOrders(`${binanceSymbol}`, (error, response, symbol) => {
        console.info(symbol+" cancel response:", response);
    });
}
async function getAllBinanceBalances(){
    await binanceUS.balance((error, balances) => {
        if ( error ) return console.error(error, 'in balance why?', );
        console.info("balances()", balances);
       // console.info(process.env.SYMBOL," balance: ", balances[process.env.SYMBOL].available);

        global.tradingData.amount = balances[`${asset}`]
        let amount = +$$(
            $(balances[process.env.SYMBOL].available),
            subtractPercent(10)).toNumber().toFixed(2)
        console.log(process.env.SYMBOL, 'amount after subtract 10% and dropped decimal to 2 places line 74', amount, balances[process.env.SYMBOL].available)
    });
}
//getAllBinanceBalances()
const sma = require('trading-indicator').sma
async function getSMANine(s, i){
      console.log(s, 'in sma 9')

}

async function sellAssetOnBinance(symbol, quantity, price){
    binanceUS.websockets.depthCache([`${binanceSymbol}`], (symbol, depth) => {
        let bids = binanceUS.sortBids(depth.bids);
        global.tradingData.sellPrice = +binanceUS.first(bids);
        //console.info("last updated: " + "new sell", global.tradingData.sellPrice);
    });
    return await binanceUS.sell(symbol, quantity, global.tradingData.sellPrice).then(resp =>{
        console.log(resp.symbol, 'placed order on Binance', resp.status)
    }).catch(err =>{
        console.log(err, 'placed sell order', symbol, quantity, price)
    })
}
async function buyAssetOnBinance(symbol){
    binanceUS.websockets.depthCache([`${binanceSymbol}`], (symbol, depth) => {
        let asks = binanceUS.sortAsks(depth.asks);
        global.tradingData.buyPrice = +binanceUS.first(asks);
       // console.info("last updated: " + "new buy", global.tradingData.buyPrice);
    });
    let fixedNumber = global.myBalances.buyingPower / global.tradingData.buyPrice
   let quantity = +$$(
       $(fixedNumber),
       subtractPercent(8)).toNumber().toFixed(2);
    return await binanceUS.buy(symbol, quantity, global.tradingData.buyPrice).then(resp =>{
        console.log('placed order on Binance', resp)
    }).catch(err =>{
        console.log(err, ' buying on binance', symbol, quantity, global.tradingData.buyPrice)
    })
}
function sellOrderPromise(symbol, quantity){
    return new Promise((resolve, reject) =>{
        sellAssetOnBinance(symbol, quantity).then(data =>{
            if(data === 200){
                console.log('returned order data =', data)
                resolve(data)
            }else{
                reject(data)
            }
        })
    })
}
function buyOrderPromise(symbol){
    console.log('inside buy order function =',symbol)
    return new Promise((resolve, reject) =>{
        buyAssetOnBinance(symbol).then(data =>{
            if(data === 200){
                console.log('returned order data =', data)
                resolve(data)
            }else{
                reject(data)
            }
        }).catch(err =>{
            console.log('fucking buying error!!!', err)
        })
    })
}
async function getBalanceAndSellAsset(asset, price) {
    let binanceSymbol = asset + 'USD'
    await binanceUS.balance((error, balances) => {
        if (error) return console.error(error, 'in balance why?',);
        //console.info("balances()", balances);
        console.info(asset, " balance: ", balances[`${asset}`].available);
        global.tradingData.amount = balances[`${asset}`]
        let amount = +$$(
            $(balances[`${asset}`].available),
            subtractPercent(10)).toNumber().toFixed(2)
        let value = price * amount
        if (value > 20) {
            console.info(asset, 'VALUE', value, " balance: ", balances[`${asset}`].available);
            sellOrderPromise(binanceSymbol, amount, global.tradingData.sellPrice).then(resp => {
                console.log(resp.symbol, 'sold for ', resp.price, 'status', resp.status)
                scanMarket(asset)
            }).catch(err => {
                getAllBinanceBalances()
                console.info(err,'sell order',asset, " balance: ", balances[`${asset}`].available);
                let amount = +$$(
                    $(balances[`${asset}`].available),
                    subtractPercent(10)).toNumber().toFixed(2)
                let value = price * amount
                if (value > 20) {
                    sellOrderPromise(binanceSymbol, amount).then(resp => {
                        console.log(resp.symbol, 'sold for ', resp.price, 'status', resp.status)
                        scanMarket(asset)
                    }).catch(err => {
                        console.info(err,'sell order',asset, " balance: ", balances[`${asset}`].available, 'params', binanceSymbol, amount, global.tradingData.sellPrice);
                    })
                } else{
                    return 'no asset left'
                }
            })

            console.log(asset, 'amount after subtract 10% and dropped decimal to 2 places because binance are cheaters', amount, 'value of trade', value)
            return amount
        }
    })
}
/*async function scanMarket(asset) {
    let binanceSymbol = process.env.SYMBOL + 'USD'
    let symbolUSDT = asset + 'USDT'
    const i = '1m'
    //getAssetsOwned(binanceSymbol).then()
        binanceUS.websockets.candlesticks([`${binanceSymbol}`], "1m", (candlesticks) => {
            let {e: eventType, E: eventTime, s: symbol, k: ticks} = candlesticks;
            let {
                o: open,
                h: high,
                l: low,
                c: close,
                v: volume,
                n: trades,
                i: interval,
                x: isFinal,
                q: quoteVolume,
                V: buyVolume,
                Q: quoteBuyVolume
            } = ticks;
            console.info(symbol + " " + interval + " candlestick update", eventTime, 'vs', rawUtcTimeNow, 'close', +close);
       this.runningAverage = 0;
       this.last20closes = []
       if (this.last20closes.length >= 20){
           let sum = _.sum(this.last20closes)
           console.log('last 20 closes added =', sum)
           const averageOflAst20Closes = sum / 20;
           console.log('average of last 20 closes ', averageOflAst20Closes)
       }
        });


}*/
async function getMarketData(){
   await binanceUS.websockets.chart("MANAUSD", "1m", (symbol, interval, chart) => {
        let tick = binanceUS.last(chart);
        const lastClose = chart[tick].close;
        const nextCloseTimeStamp = Number(tick) + 60000


        console.info( 'last tickers volume',chart[tick].volume, 'next close', nextCloseTimeStamp);

        // Optionally convert 'chart' object to array:
         let ohlc = binanceUS.ohlc(chart);
         let last20 = ohlc.close.slice(-15)
       let averageOfLast20 = _.sum(last20) / 15
         console.info(symbol, 'average of 20 closes', averageOfLast20);
        console.info(symbol+" last price: "+lastClose)
    });
}

getAllBinanceBalances().then(balance =>{
    setInterval(function (){
        getMarketData();
   /* scanMarket(process.env.SYMBOL).then(no =>{
        const utils = new Utils()
        cancelAllOpenOrders().then()
        getBuyingPower().then()
       // getAllBinanceBalances().then()
        utils.getFormattedDate()
        console.log(process.env.SYMBOL, 'in interval', global.tradingData, 'balances',global.myBalances,'buying power', global.myBalances.buyingPower)
    })*/
}, 5000)
})

buyingPowerPromise()
console.log('my buying power', global.myBalances.buyingPower)


console.log('im alive trading asset=', asset)
app.listen(process.env.PORT);
